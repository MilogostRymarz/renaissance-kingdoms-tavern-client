package com.studentProject.ChatKrolestwa.OperationsOutsideTavern;

import org.apache.http.Header;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public class HTTPLoginSession implements Closeable {
    private final String login, password;
    private final String browserUserAgent;
    private final CookieStore magazynCiastek;
    private final CloseableHttpClient klientHttp;

    public HTTPLoginSession(final String login, final String password){
        this.login    = login;
        this.password = password;
        this.browserUserAgent = "Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0";
        this.magazynCiastek = new BasicCookieStore();
        this.klientHttp = HttpClients.custom()
                .setUserAgent(browserUserAgent)
                .setDefaultCookieStore(magazynCiastek)
                .build();
    }

    public boolean login(){
        boolean isLogged = false;
//        I need cookie before login request
        final HttpUriRequest requestLoginPage = RequestBuilder.get()
                .setCharset(Charset.forName("UTF-8"))
                .setUri("https://www.krolestwa.com/")
                .setHeader("Accept-Language:","pl,en-US;q=0.7,en;q=0.3")
                .build();
        try(CloseableHttpResponse responseForLoginPage = klientHttp.execute(requestLoginPage)){
            EntityUtils.consumeQuietly(responseForLoginPage.getEntity());
        } catch (IOException e) {
            new RuntimeException(e.getMessage());
            System.err.println("Błąd w login() " + e.getMessage());
        }

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {}

        HttpUriRequest requestOfLogging = RequestBuilder.post()
                .setCharset(Charset.forName("UTF-8"))
                .setHeader("Referer","https://www.krolestwa.com/")
                .setHeader("Accept-Language:","pl,en-US;q=0.7,en;q=0.3")
                .setHeader("Accept-Encoding","gzip, deflate, br")
                .setHeader("Content-Type","application/x-www-form-urlencoded")
                .setUri("https://www.krolestwa.com/ConnexionKC.php")
                .addParameter("login",login)
                .addParameter("password",password)
                .build();

        try(CloseableHttpResponse responseForLogin = klientHttp.execute(requestOfLogging)){
            final Header[] naglowki = responseForLogin.getAllHeaders();
            for (Header header : naglowki){
                if (header.getName().contains("Location")) {
                    if (header.getValue().contains("EcranPrincipal.php") || header.getValue().contains("TableauDeBord.php"))
                        isLogged = true;
                }
            }
            isLogged = isLogged && responseForLogin.getStatusLine().getStatusCode() == 302;
            final InputStream content = responseForLogin.getEntity().getContent();
            final int availableContent = content.available();
            final byte[] contentInBytes = new byte[availableContent];
            content.read(contentInBytes,0,availableContent);
            String odpowiedź = new String(contentInBytes,Charset.forName("UTF-8"));
            EntityUtils.consumeQuietly(responseForLogin.getEntity());
        } catch (IOException e) {
            new RuntimeException(e.getMessage());
            System.err.println("Błąd w login() " + e.getMessage());
        }

        return isLogged;
    }

    private void logout(){
        HttpUriRequest requestForLogout = RequestBuilder.get()
                .setUri("https://www.krolestwa.com/Deconnexion.php")
                .setHeader("Accept-Language:","pl,en-US;q=0.7,en;q=0.3")
                .setHeader("Referer","https://www.krolestwa.com/EcranPrincipal.php")
                .build();
        try(CloseableHttpResponse responseForLoginPage = klientHttp.execute(requestForLogout)){
            EntityUtils.consumeQuietly(responseForLoginPage.getEntity());
        } catch (IOException e) {
            System.err.println("Błąd w logout() " + e.getMessage());
        }
    }

    public void close() {
        logout();
    }
}
