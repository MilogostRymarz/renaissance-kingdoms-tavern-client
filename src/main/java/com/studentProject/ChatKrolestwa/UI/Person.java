package com.studentProject.ChatKrolestwa.UI;

import com.studentProject.ChatKrolestwa.OperationsInsideTavern.ParsedPlayerPresence;

public class Person {
    private final String name;
    private       int    chairNumber;
    private final ParsedPlayerPresence.Gender gender;


    public Person(String name, int chairNumber, ParsedPlayerPresence.Gender gender) {
        this.name = name;
        this.chairNumber = chairNumber;
        this.gender = gender;
    }


    public String getName() {
        return name;
    }

    public int getChairNumber() {
        return chairNumber;
    }

    public void setChairNumber(int chairNumber) {
        this.chairNumber = chairNumber;
    }

    public ParsedPlayerPresence.Gender getGender() {
        return gender;
    }

    @Override
    public String toString(){
        if (getChairNumber() != -1)
            return String.format("[%s] %s is sitting at chair %d\n",
                    getGender().equals(ParsedPlayerPresence.Gender.men) ? "M" :
                            (getGender().equals(ParsedPlayerPresence.Gender.women) ? "W" : ""),
                    getName(),
                    getChairNumber()
            );
        else
            return String.format("[%s] %s is sitting\n",
                    getGender().equals(ParsedPlayerPresence.Gender.men) ? "M" :
                            (getGender().equals(ParsedPlayerPresence.Gender.women) ? "W" : ""),
                    getName()
            );
    }
}
