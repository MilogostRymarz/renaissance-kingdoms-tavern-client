package com.studentProject.ChatKrolestwa.UI;

import com.studentProject.ChatKrolestwa.OperationsInsideTavern.*;
import com.studentProject.ChatKrolestwa.OperationsOutsideTavern.HTTPLoginSession;
import com.studentProject.ChatKrolestwa.Credentials.*;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;


import java.io.IOException;
import java.util.*;


public class ChatMainWindow extends Application {
//    private final TextArea textListOfPersons;
    private final ListView<Person> listViewOfPersons;
    private final List<ParsedPlayerPresence>     listOfPersons;
    private final Label labelOfClientState;
    private final TextArea textReceivedList;
    private final LimitedMessageQueue listReceivedList;
    private final TextField textEnterMessage;
    private final Button buttonConnect, buttonSettings, buttonBuyBeer,buttonSend, buttonWhisper, buttonMenu;
    private final VBox innerLeftVBox,innerRightVBox;
    private final HBox lowerHBox, higherHBox;
    private final VBox outerVBox;
    private final Scene primaryStageScene;
    private       String login, password, encodedPassword;
    private       ParsedTavernMenu parsedTavernMenu;
    private SynchronisedConnectionToTavern synchronisedConnectionToTavern;
    private       Thread threadKeepAliver;
    private       Thread threadReceiver;

    //        12223
//        47172
//        częstochowa - 6258
    private int tavernNumber = 12223;


    public ChatMainWindow(){
//        textListOfPersons = new TextArea("List of persons in tavern");
        listViewOfPersons = new ListView<>();
        labelOfClientState = new Label("Not connected");
        textReceivedList = new TextArea("");
        textReceivedList.setPrefWidth(740);
        textEnterMessage = new TextField("Enter your message...");
        buttonConnect = new Button("Connect");
        buttonSettings = new Button("Settings");
        buttonSend = new Button("Send");
        buttonSend.setDisable(true);
        buttonWhisper = new Button("Whisper");
        buttonWhisper.setDisable(true);
        buttonMenu = new Button("Menu");
        buttonMenu.setDisable(true);
        buttonBuyBeer = new Button("Buy beer for:");
        buttonBuyBeer.setDisable(true);
        innerLeftVBox = new VBox(3, textReceivedList, textEnterMessage);
        HBox sendingButtonsHBox = new HBox(buttonSend,buttonWhisper);
        innerRightVBox = new VBox(3, buttonBuyBeer, listViewOfPersons, sendingButtonsHBox);
        lowerHBox = new HBox(3, innerLeftVBox,innerRightVBox);
        higherHBox = new HBox(10, labelOfClientState,buttonConnect,buttonMenu,buttonSettings);
        outerVBox = new VBox(higherHBox, lowerHBox);
        primaryStageScene = new Scene(outerVBox,900,550);
        listOfPersons = new LinkedList();
        listReceivedList = new LimitedMessageQueue(100);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Optional<String[]> optStrings = (new ReadCredentials()).read();
        if (optStrings.isPresent()){
            final String[] strings = optStrings.get();
            login = strings[0];
            password = strings[1];
            encodedPassword = strings[2];
            if (strings[3] != null)
                tavernNumber = Integer.parseInt(strings[3]);
            else
                tavernNumber = 0;
            createMainWindow(primaryStage);
        } else {
            createMainWindow(primaryStage);
            createAndShowSettingsWindow();
        }
    }

    public void addMessageToMessageArea(String message){
        listReceivedList.offer(message);
        updateMessageArea();
    }

    private void updateMessageArea() {
        final StringBuilder stringBuilder = new StringBuilder();
        Iterator<String> descendingStringIterator = listReceivedList.descendingIterator();
        for (Iterator<String> it = descendingStringIterator; it.hasNext(); ) {
            String message = it.next();
            stringBuilder.append(message);
            stringBuilder.append('\n');
        }
        textReceivedList.setText(stringBuilder.toString());
    }

    public void addOrUpdatePersonToPresentPersonsList(ParsedPlayerMessage interpretedPlayerMessage) {
        for (int i = 0; i < listOfPersons.size(); ++i){
            final ParsedPlayerPresence parsedPlayerPresence = listOfPersons.get(i);
            if (parsedPlayerPresence.getPersonName().equals(interpretedPlayerMessage.getAuthorOfMessage())){
                return;
            }
        }
        listOfPersons.add(new ParsedPlayerPresence() {
            @Override
            public String getPersonName() {
                return interpretedPlayerMessage.getAuthorOfMessage();
            }

            @Override
            public int getChairNumber() {
                return -1;
            }

            @Override
            public Gender getGender() {
                return Gender.notSpecified;
            }
        });
//        updateTextListOfPersons();
        updateListViewOfPersons(interpretedPlayerMessage.getAuthorOfMessage());
    }

    public void addOrUpdatePersonToPresentPersonsList(ParsedPlayerPresence addThisPresence){
        for (int i = 0; i < listOfPersons.size(); ++i){
            final ParsedPlayerPresence parsedPlayerPresence = listOfPersons.get(i);
            if (parsedPlayerPresence.getPersonName().equals(addThisPresence.getPersonName())){
                listOfPersons.set(i, addThisPresence);
//                updateTextListOfPersons();
                return;
            }
        }
        listOfPersons.add(addThisPresence);
//        updateTextListOfPersons();
        updateListViewOfPersons(addThisPresence.getPersonName());
    }

    public void removePersonFromPresentPersonsList(String personName){
        for (int i = 0; i < listOfPersons.size(); ++i){
            final ParsedPlayerPresence parsedPlayerPresence = listOfPersons.get(i);
            if (parsedPlayerPresence.getPersonName().equals(personName)  && !parsedPlayerPresence.getPersonName().equals(login)){
                listOfPersons.remove(i);
//                updateTextListOfPersons();
                updateListViewOfPersons(personName);
                break;
            }
        }
    }

    public void updateTavernMenu(ParsedTavernMenu parsedTavernMenu) {
        if (parsedTavernMenu != null) {
            this.parsedTavernMenu = parsedTavernMenu;
            this.buttonMenu.setDisable(false);
        }
    }

/*    private void updateTextListOfPersons() {
        final StringBuilder tmpStrBld = new StringBuilder();
        for (ParsedPlayerPresence parsedPlayerPresence : listOfPersons) {
            if (parsedPlayerPresence.getChairNumber() != -1)
                tmpStrBld.append(String.format("[%s] %s is sitting at chair %d\n",
                        parsedPlayerPresence.getGender().equals(ParsedPlayerPresence.Gender.men) ? "M" :
                                (parsedPlayerPresence.getGender().equals(ParsedPlayerPresence.Gender.women) ? "W" : ""),
                        parsedPlayerPresence.getPersonName(),
                        parsedPlayerPresence.getChairNumber()
                ));
            else
                tmpStrBld.append(String.format("[%s] %s is sitting\n",
                        parsedPlayerPresence.getGender().equals(ParsedPlayerPresence.Gender.men) ? "M" :
                                (parsedPlayerPresence.getGender().equals(ParsedPlayerPresence.Gender.women) ? "W" : ""),
                        parsedPlayerPresence.getPersonName()
                ));
        }
        textListOfPersons.setText(tmpStrBld.toString());
    }*/

    private void updateListViewOfPersons(){
        this.listViewOfPersons.getItems().clear();
        for (ParsedPlayerPresence parsedPlayerPresence : listOfPersons){
            final Person person = new Person(
                    parsedPlayerPresence.getPersonName(),
                    parsedPlayerPresence.getChairNumber(),
                    parsedPlayerPresence.getGender());
            listViewOfPersons.getItems().add(person);
        }
    }

    private void updateListViewOfPersons(String playername) {
        ParsedPlayerPresence searchedPresence = null;
        for (ParsedPlayerPresence parsedPlayerPresence : listOfPersons) {
            if (parsedPlayerPresence.getPersonName().equals(playername))
                searchedPresence = parsedPlayerPresence;
        }
        Person searchedPersonInListView = null;
        int searchedPositionInListView = -1;
        for (Person person : this.listViewOfPersons.getItems()) {
            if (person.getName().equals(playername)) {
                searchedPersonInListView = person;
                searchedPositionInListView = listViewOfPersons.getItems().indexOf(person);
            }
        }
//        remove person
        if (searchedPresence == null && searchedPersonInListView != null)
            this.listViewOfPersons.getItems().remove(searchedPersonInListView);
//        update person by replacing object
        if (searchedPresence != null && searchedPersonInListView != null)
            this.listViewOfPersons.getItems().set(searchedPositionInListView,
                    new Person(searchedPresence.getPersonName(),
                            searchedPresence.getChairNumber(),
                            searchedPresence.getGender()
                    ));
//        add person
        if (searchedPresence != null && searchedPersonInListView == null) {
            this.listViewOfPersons.getItems().add(
                    new Person(searchedPresence.getPersonName(),
                            searchedPresence.getChairNumber(),
                            searchedPresence.getGender()
                    ));
        }
    }

    private void createAndShowSettingsWindow(){
        final TextField txLogin, txPassword,txEncodedPassword, txTavernNumber;
        txLogin = new TextField(this.login);
        txPassword = new TextField(this.password);
        txEncodedPassword = new TextField(this.encodedPassword);
        txTavernNumber = new TextField(String.valueOf(this.tavernNumber));
        final Label lbLogin, lbPassword, lbEncodedPassword, lbTavernNumber;
        lbLogin = new Label("Login: ");
        lbPassword = new Label("Password: ");
        lbEncodedPassword = new Label("Encoded pass: ");
        lbTavernNumber = new Label("Tavern number: ");
        final Button btSave = new Button("Save config to file");
        final WriteCredentials writeCredentials = new WriteCredentials();
        btSave.setOnMouseClicked(event -> {
            this.login = txLogin.getText();
            this.password = txPassword.getText();
            this.encodedPassword = txEncodedPassword.getText();
            this.tavernNumber = Integer.parseInt(txTavernNumber.getText());
            writeCredentials.write(this.login,this.password,this.encodedPassword,this.tavernNumber);
            event.consume();
        });
        final VBox vbLogin, vbPassword,vbEncodedPassword, vbTavernNumber;
        vbLogin = new VBox(lbLogin,txLogin);
        vbPassword = new VBox(lbPassword,txPassword);
        vbEncodedPassword = new VBox(lbEncodedPassword,txEncodedPassword);
        vbTavernNumber = new VBox(lbTavernNumber, txTavernNumber);
        final HBox settingsHBox = new HBox(vbLogin,vbPassword,vbEncodedPassword, vbTavernNumber);
        final VBox settingsRoot = new VBox(settingsHBox,btSave);
        Scene settingsScene = new Scene(settingsRoot);
        Stage settingsWindow = new Stage();
        settingsWindow.setTitle("Settings");
        settingsWindow.setScene(settingsScene);
        settingsWindow.show();
    }



    private void createMainWindow(final Stage primaryStage){
//        textListOfPersons.setDisable(false);
//        textListOfPersons.setEditable(false);
//        textListOfPersons.setPrefWidth(150);
        textReceivedList.setDisable(false);
        textReceivedList.setPrefHeight(500);
        textReceivedList.setEditable(false);
        textReceivedList.setFont(Font.font(14));
        listViewOfPersons.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        innerLeftVBox.setPadding(new Insets(5));
        innerRightVBox.setPadding(new Insets(5));
        lowerHBox.setPadding(new Insets(5));
        buttonSettings.setOnMouseClicked(event -> createAndShowSettingsWindow());
        buttonConnect.setOnMouseClicked(event -> joinTavern());
        buttonSend.setOnAction(event -> {
            try {
                synchronisedConnectionToTavern.sendRegularMessage(textEnterMessage.getText());
            } catch (IOException e) {
                synchronisedConnectionToTavern.close();
            }
            textEnterMessage.clear();
            event.consume();
        });
        buttonWhisper.setOnMouseClicked(event -> {
            try {
                for (Person person : this.listViewOfPersons.getSelectionModel().getSelectedItems()){
                    synchronisedConnectionToTavern.sendWhisperedMessage(textEnterMessage.getText(),person.getName());
                }
            } catch (IOException e) {
                synchronisedConnectionToTavern.close();
            }
            textEnterMessage.clear();
            event.consume();
        });
        buttonBuyBeer.setOnMouseClicked(event -> {
            try {
                for (Person person : this.listViewOfPersons.getSelectionModel().getSelectedItems()){
                    synchronisedConnectionToTavern.sendRegularMessage(
                            String.format("/verre %s",person.getName())
                    );
                }
            } catch (IOException e) {
                synchronisedConnectionToTavern.close();
            }
            event.consume();
        });
        textEnterMessage.setText("");
        textEnterMessage.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                buttonSend.fire();
            }
            event.consume();
        });
        buttonMenu.setOnAction(event -> {
            createAndShowMenuWindow();
            event.consume();
        });

        primaryStage.setTitle("Unofficial Renaissance Kingdoms Tavern client");
        primaryStage.setScene(primaryStageScene);
        primaryStage.show();
    }

    private void createAndShowMenuWindow() {
        if (parsedTavernMenu == null)
            return;

        final HBox beerHBox;
        {
            Label beerLabel = new Label("Beer price: " + ((double)parsedTavernMenu.getPriceOfBeer()/100));
            Button beerForMeButton = new Button("Beer for me");
            beerForMeButton.setOnAction(event -> {
                try {
                    this.synchronisedConnectionToTavern.sendRegularMessage(String.format("/verre %s",this.login));
                } catch (IOException e) {
                    synchronisedConnectionToTavern.close();
                }
                event.consume();
            });
            Button beerForAllButton = new Button("Beer for all");
            beerForAllButton.setOnAction(event -> {
                try {
                    this.synchronisedConnectionToTavern.sendRegularMessage("/verre tous");
                } catch (IOException e) {
                    synchronisedConnectionToTavern.close();
                }
                event.consume();
            });
            HBox buttonsForBeerHBox = new HBox(5,beerForMeButton,beerForAllButton);
            VBox beerVBox = new VBox(1,beerLabel,buttonsForBeerHBox);
            beerHBox = new HBox(beerVBox);
        }
        final int numberOfMeals = parsedTavernMenu.getNamesOfMeals().size();
        final VBox innerLeftMenuVbox = new VBox(beerHBox);
        final VBox innerRightMenuVbox = new VBox();
        final HBox menuRoot = new HBox(innerLeftMenuVbox,innerRightMenuVbox);
        final ObservableList<Node> innerLeftMenuVBoxChildren = innerLeftMenuVbox.getChildren();
        innerLeftMenuVBoxChildren.add(new HBox(new Label(String.format("%d kinds of meals available",numberOfMeals))));
        for (int i = 0; i<numberOfMeals; ++i){
            Label mealNameLabel = new Label(String.format("Meal %d is called: %s",
                    i+1,
                    parsedTavernMenu.getNamesOfMeals().get(i)));
            Label mealPriceLabel = new Label(String.format("Price of meal %d: %s",
                    i+1,
                    (double)parsedTavernMenu.getPricesOfMeals().get(i)/100));
            StringBuilder ingredientsBuilder = new StringBuilder("Ingredients: ");
            for (Integer ingredient : parsedTavernMenu.getIngredientsOfMeals().get(i)){
                ingredientsBuilder.append(ingredient);
                ingredientsBuilder.append('\t');
            }
            Label mealIngredientsLabel = new Label(ingredientsBuilder.toString());
            Button mealButton = new Button(String.format("Buy meal %d",i+1));
            final int finalI = i + 1;
            mealButton.setOnAction(event -> {
                try {
                    this.synchronisedConnectionToTavern.sendRegularMessage(String.format("/repas %d", finalI));
                } catch (IOException e) {
                    synchronisedConnectionToTavern.close();
                }
                event.consume();
            });
            VBox mealVbox = new VBox(mealNameLabel,mealPriceLabel,mealIngredientsLabel);
            innerRightMenuVbox.getChildren().add(mealButton);
            final HBox mealHBox = new HBox(mealVbox);
            innerLeftMenuVBoxChildren.add(mealHBox);
        }
        final Scene menuScene = new Scene(menuRoot,320,190);
        final Stage menuWindow = new Stage();
        menuWindow.setTitle("Menu");
        menuWindow.setScene(menuScene);
        menuWindow.show();
    }

    private void connectToAccount(){
        boolean success = false;
        if (this.login == null)
            return;
        try (HTTPLoginSession HTTPLoginSession = new HTTPLoginSession(login,password)){
            labelOfClientState.setText("Connecting...");
            success = HTTPLoginSession.login();
            labelOfClientState.setText("Connected");

        }
        if (success)
            labelOfClientState.setText("Connection successful");
        else
            labelOfClientState.setText("Connection failed");
    }

    private void joinTavern() {
        if (this.synchronisedConnectionToTavern != null && this.synchronisedConnectionToTavern.isConnected()) {
            this.listReceivedList.clear();
            this.listOfPersons.clear();
            threadKeepAliver.interrupt();
            threadKeepAliver = null;
            threadReceiver.interrupt();
            threadReceiver = null;
            synchronisedConnectionToTavern.close();
            buttonConnect.setText("Connect");
            labelOfClientState.setText("Not connected");
            this.buttonMenu.setDisable(true);
            return;
        }



        if (login == null || tavernNumber == 0) {
            createAndShowSettingsWindow();
            return;
        }
        try {
            synchronisedConnectionToTavern = new SynchronisedConnectionToTavern(tavernNumber, login, encodedPassword,"ns36946.ovh.net",8080);
        } catch (IOException e) {
            return;
        }

        if (synchronisedConnectionToTavern.connect()) {
            this.listViewOfPersons.getItems().clear();
            this.labelOfClientState.setText("I am connected to tavern no " + String.valueOf(tavernNumber));
            this.buttonConnect.setText("Disconnect");
            this.buttonSend.setDisable(false);
            this.buttonWhisper.setDisable(false);
            this.buttonBuyBeer.setDisable(false);
        } else
            return;

        threadKeepAliver = new Thread(new Runnable() {
            @Override
            public void run() {
                KeepAliveThr keepAliveThr = new KeepAliveThr(synchronisedConnectionToTavern);
                keepAliveThr.mainLoop();
            }
        });
        threadKeepAliver.setDaemon(true);

        threadReceiver = new Thread(
                new ReceiverThr(synchronisedConnectionToTavern,this)
        );
        threadReceiver.setDaemon(true);

        threadKeepAliver.start();
        threadReceiver.start();
    }


}
