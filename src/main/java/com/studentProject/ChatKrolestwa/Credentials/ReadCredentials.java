package com.studentProject.ChatKrolestwa.Credentials;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Scanner;

public class ReadCredentials {
    final File settingsFile;
    private final String directoryName;

    public ReadCredentials(){
        this.directoryName = ".RenaissanceKingdomsTavernClient";
        Path pathToCredentials = Paths.get(System.getProperty("user.home"),directoryName, "config.txt");
        this.settingsFile = pathToCredentials.toFile();
    }

    public Optional<String[]> read(){
        if (settingsFile.exists()){
            try (Scanner scanner = new Scanner(settingsFile)){
                final String[] strings = new String[4];
                int i = 0;
                //                read login
                //                read password
                //                read encoded tavern password
//                tavernNumber
                while (scanner.hasNextLine() && i<4){
                    strings[i] = scanner.nextLine();
                    ++i;
                }
                return Optional.of(strings);
            } catch (FileNotFoundException e) {

            }
        }
        return Optional.empty();
    }
}
