package com.studentProject.ChatKrolestwa.Credentials;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

public class WriteCredentials {
    private final File settingsFile;
    private final String directoryName;

    public WriteCredentials(){
        this.directoryName = ".RenaissanceKingdomsTavernClient";
        Path pathToCredentials = Paths.get(System.getProperty("user.home"),directoryName, "config.txt");
        this.settingsFile = pathToCredentials.toFile();
    }

    public void write(String login, String password, String encodedPassword, int tavernNumber){
        if (login == null || login.equals("") || password == null || password.equals(""))
            return;

        final String strTavernNumber = String.valueOf(tavernNumber);

        File directoryFile = new File(System.getProperty("user.home") + File.separator + directoryName);
        if (!directoryFile.exists())
            directoryFile.mkdirs();

        if (!settingsFile.exists())
            try {
                settingsFile.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException("Problem while creating settingsFile");
            }

        try (PrintWriter settingsWriter = new PrintWriter(settingsFile)) {
            settingsWriter.println(login);
            settingsWriter.println(password);
            settingsWriter.println(encodedPassword);
            settingsWriter.println(tavernNumber);
        } catch (FileNotFoundException e) {
            System.err.println("Error while writing to config settingsFile");
        }
    }
}