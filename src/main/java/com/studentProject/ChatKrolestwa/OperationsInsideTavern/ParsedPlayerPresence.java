package com.studentProject.ChatKrolestwa.OperationsInsideTavern;

public interface ParsedPlayerPresence {
    public enum Gender {
        men,
        women,
        notSpecified
    };

    public abstract String getPersonName();
    public abstract int    getChairNumber();
    public abstract Gender getGender();
}
