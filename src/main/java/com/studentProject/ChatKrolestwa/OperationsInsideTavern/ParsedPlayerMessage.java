package com.studentProject.ChatKrolestwa.OperationsInsideTavern;

public interface ParsedPlayerMessage {
    String getMessage();
    int getTypeOfMessage();
    String getAuthorOfMessage();
    String getRecipientOfMessage();
    boolean isHistory();
}
