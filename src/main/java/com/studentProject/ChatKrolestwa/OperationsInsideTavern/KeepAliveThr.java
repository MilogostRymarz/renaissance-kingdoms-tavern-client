package com.studentProject.ChatKrolestwa.OperationsInsideTavern;

import java.io.IOException;


public class KeepAliveThr {
    private final SynchronisedConnectionToTavern synchronisedConnectionToTavern;

    public KeepAliveThr(SynchronisedConnectionToTavern synchronisedConnectionToTavern){
        this.synchronisedConnectionToTavern = synchronisedConnectionToTavern;
    }

    public void mainLoop() {
        int i = 0;
        while (true) {
            try {
                ++i;
                waitHalfSecond();
                if (i % 60 == 0) {
                    i = 0;
                    keepAlive();
                }

            } catch (IOException e) {
                synchronisedConnectionToTavern.close();
                return;
            }
        }
    }

    private void keepAlive() throws IOException {
        synchronisedConnectionToTavern.keepAlive();
    }

    private void waitHalfSecond(){
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {

        }
    }
}
