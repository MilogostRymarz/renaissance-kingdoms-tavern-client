package com.studentProject.ChatKrolestwa.OperationsInsideTavern;


import com.studentProject.ChatKrolestwa.TextProtocol.SimpleParseText;
import com.studentProject.ChatKrolestwa.UI.ChatMainWindow;
import javafx.application.Platform;

import java.io.IOException;
import java.util.Optional;

public final class ReceiverThr implements Runnable {
    private final SynchronisedConnectionToTavern synchronisedConnectionToTavern;
    private final ChatMainWindow mainAppWindow;


    public ReceiverThr(
            SynchronisedConnectionToTavern synchronisedConnectionToTavern,
            ChatMainWindow mainAppWindow) {
        this.synchronisedConnectionToTavern = synchronisedConnectionToTavern;
        this.mainAppWindow = mainAppWindow;
    }

    public void mainLoop() {
        try {
            while (true) {

                final Optional<String> optMessage = receiveMessageFromSocket();
                if (!optMessage.isPresent()) {
                    if (synchronisedConnectionToTavern.isThereBytesOfPartialMessage()) {
                        Thread.yield();
                        continue;
                    }
                    try {
                        Thread.sleep(100);
                        continue;
                    } catch (InterruptedException e) {
                        continue;
                    }
                }

                final SimpleParseText simpleParseText = new SimpleParseText(optMessage.get());

                simpleParseText.getInterpretedPersonMessage().ifPresent(interpretedPlayerMessage -> {
                    switch (interpretedPlayerMessage.getTypeOfMessage()) {
                        case 1: { // regular message
                            final String message;
                            if (interpretedPlayerMessage.isHistory())
                                message = String.format("%s wrote: %s",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getMessage());
                            else
                                message = String.format("%s writes: %s",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getMessage());
                            showMessageInGUI(message);
                        }
                        break;
                        case 2: { // Role playing-style message
                            final String message;
                            if (interpretedPlayerMessage.isHistory())
                                message = String.format("%s's character wrote %s",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getMessage());
                            else
                                message = String.format("%s's character writes %s",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getMessage());
                            showMessageInGUI(message);
                        }
                        break;
                        case 3: { // somebody get into tavern
                            if (interpretedPlayerMessage.isHistory()) {
                                showMessageInGUI(String.format("%s sits in tavern", interpretedPlayerMessage.getAuthorOfMessage()));
                            } else {
                                showMessageInGUI(String.format("%s sits in tavern", interpretedPlayerMessage.getAuthorOfMessage()));
                                addPersonInGUI(interpretedPlayerMessage);
                            }
                        }
                        break;
                        case 4: { // somebody get out of tavern - probably happy
                            if (interpretedPlayerMessage.isHistory()) {
                                showMessageInGUI(String.format(
                                        "%s gets out of tavern",
                                        interpretedPlayerMessage.getAuthorOfMessage()));
                            } else {
                                removePersonInGUI(interpretedPlayerMessage.getAuthorOfMessage());
                                showMessageInGUI(String.format(
                                        "%s gets out of tavern",
                                        interpretedPlayerMessage.getAuthorOfMessage()));
                            }
                        }
                        break;
                        case 5: { // somebody get out of tavern - probably angry
                            if (interpretedPlayerMessage.isHistory()) {
                                showMessageInGUI(String.format(
                                        "%s gets out of tavern",
                                        interpretedPlayerMessage.getAuthorOfMessage()));
                            } else {
                                removePersonInGUI(interpretedPlayerMessage.getAuthorOfMessage());
                                showMessageInGUI(String.format(
                                        "%s gets out of tavern",
                                        interpretedPlayerMessage.getAuthorOfMessage()));
                            }
                        }
                        break;
                        case 7: { // beer for %s
                            if (interpretedPlayerMessage.isHistory()) {
                                showMessageInGUI(String.format(
                                        "%s gave beer for %s",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getRecipientOfMessage()));
                            } else {
                                showMessageInGUI(String.format(
                                        "%s gives beer for %s",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getRecipientOfMessage()));
                            }
                        }
                        break;
                        case 9: { // when I buy beer for somebody, but he refuses to drink
                            if (interpretedPlayerMessage.isHistory()) {
                                showMessageInGUI(String.format(
                                        "%s gave beer for %s, but %s didn't wanted drinkg",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getRecipientOfMessage(),
                                        interpretedPlayerMessage.getRecipientOfMessage()));
                            } else {
                                showMessageInGUI(String.format(
                                        "%s gives beer for %s, but %s do not want to drink",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getRecipientOfMessage(),
                                        interpretedPlayerMessage.getRecipientOfMessage()));
                            }
                        }
                        break;
                        case 10: { // beer for everybody
                            if (interpretedPlayerMessage.isHistory()) {
                                showMessageInGUI(String.format(
                                        "%s gave beer for everybody",
                                        interpretedPlayerMessage.getAuthorOfMessage()));
                            } else {
                                showMessageInGUI(String.format(
                                        "%s gives beer for everybody",
                                        interpretedPlayerMessage.getAuthorOfMessage()));
                            }
                        }
                        break;
                        case 11: {
                            if (interpretedPlayerMessage.isHistory())
                                showMessageInGUI(String.format("%s wanted to give beer for everybody, but nobody wanted to drink",
                                        interpretedPlayerMessage.getAuthorOfMessage()
                                        ));
                            else
                                showMessageInGUI(String.format("%s wants to give beer for everybody, but nobody drinks",
                                        interpretedPlayerMessage.getAuthorOfMessage()
                                        ));
                        }
                        break;
                        case 13: { // somebody whispers to you (i saw texte and reciepient) or when I whisper to somebody
                            final String message;
                            if (interpretedPlayerMessage.isHistory())
                                message = String.format("%s whispered (type 13) to %s: ",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getRecipientOfMessage(),
                                        interpretedPlayerMessage.getMessage());
                            else
                                message = String.format("%s whispers (type 13) to %s: %s",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getRecipientOfMessage(),
                                        interpretedPlayerMessage.getMessage());
                            showMessageInGUI(message);
                        }
                        break;
                        case 15: { // somebody whispers to you (i saw texte and reciepient)
                            final String message;
                            if (interpretedPlayerMessage.isHistory())
                                message = String.format("%s whispered (type 15) to %s: ",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getRecipientOfMessage(),
                                        interpretedPlayerMessage.getMessage());
                            else
                                message = String.format("%s whispers (type 15) to %s: %s",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getRecipientOfMessage(),
                                        interpretedPlayerMessage.getMessage());
                            showMessageInGUI(message);
                        }
                        break;
                        case 18: { // $s successfully orders meal
                            if (interpretedPlayerMessage.isHistory()) {
                                showMessageInGUI(String.format(
                                        "%s ordered meal called %s",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getMessage()));
                            } else {
                                showMessageInGUI(String.format(
                                        "%s orders meal called %s",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getMessage()));
                            }
                        }
                        break;
                        case 22: { // $s orders meal, but he/she is not hungry
                            if (interpretedPlayerMessage.isHistory()) {
                                showMessageInGUI(String.format(
                                        "%s wanted to ordered meal, but was not hungry",
                                        interpretedPlayerMessage.getAuthorOfMessage()));
                            } else {
                                showMessageInGUI(String.format(
                                        "%s wanted to order meal, but is not hungry",
                                        interpretedPlayerMessage.getAuthorOfMessage()));
                            }
                        }
                        break;
                        case 99: { // somebody whispers to somebody? or malformed message
                            final String message = String.format("%s wrote malformed message: %s",
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getMessage());
                            showMessageInGUI(message);
                        }
                        break;
                        default: {
                            final String message;
                            if (interpretedPlayerMessage.isHistory())
                                message = String.format("(Type %d of message) %s wrote: %s",
                                        interpretedPlayerMessage.getTypeOfMessage(),
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getMessage());
                            else
                                message = String.format("(Type %d of message) %s writes: %s",
                                        interpretedPlayerMessage.getTypeOfMessage(),
                                        interpretedPlayerMessage.getAuthorOfMessage(),
                                        interpretedPlayerMessage.getMessage());
                            showMessageInGUI(message);
                        }
                    }
                });

                simpleParseText.getPersonInTavernInfo().ifPresent(this::addPersonInGUI);

                simpleParseText.getParsedTavernMenu().ifPresent(this::updateMenu);
                Thread.yield();
            }
        } catch (IOException e) {
            synchronisedConnectionToTavern.close();
            return;
        }
    }

    private void updateMenu(ParsedTavernMenu parsedTavernMenu) {
        Platform.runLater(() -> mainAppWindow.updateTavernMenu(parsedTavernMenu));
    }


    private Optional<String> receiveMessageFromSocket() throws IOException {
        return synchronisedConnectionToTavern.receiveMessage();
    }

    private void removePersonInGUI(String authorOfMessage) {
        Platform.runLater(() -> {
            mainAppWindow.removePersonFromPresentPersonsList(authorOfMessage);
        });
    }

    private void addPersonInGUI(ParsedPlayerMessage interpretedPlayerMessage) {
        Platform.runLater(() -> {
            mainAppWindow.addOrUpdatePersonToPresentPersonsList(interpretedPlayerMessage);
        });
    }

    private void addPersonInGUI(ParsedPlayerPresence parsedPlayerPresence){
        Platform.runLater(() -> {
            mainAppWindow.addOrUpdatePersonToPresentPersonsList(parsedPlayerPresence);
        });
    }

    private void showMessageInGUI(String message){
        if (message == null)
            return;

        Platform.runLater(() -> {
            mainAppWindow.addMessageToMessageArea(message);
        });
    }

    @Override
    public void run() {
        mainLoop();
    }
}
