package com.studentProject.ChatKrolestwa.OperationsInsideTavern;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Optional;

public class SynchronisedConnectionToTavern implements ConnectionToTavern {
    private final ConnectionToTavern connectionToTavern;

    public SynchronisedConnectionToTavern(int tavernId, String myLogin, String myPassword, InetSocketAddress inetSocketAddress) throws IOException {
        this.connectionToTavern = new SimpleConnectionToTavern(tavernId,myLogin,myPassword, inetSocketAddress);
    }

    public SynchronisedConnectionToTavern(int tavernId, String myLogin, String myPassword, String serverAddress, int serverPort) throws IOException {
        this.connectionToTavern = new SimpleConnectionToTavern(tavernId,myLogin,myPassword, serverAddress, serverPort);
    }


    @Override
    public boolean connect() {
        synchronized (connectionToTavern){
            return connectionToTavern.connect();
        }
    }

    @Override
    public void keepAlive() throws IOException {
        synchronized (connectionToTavern){
            connectionToTavern.keepAlive();
        }
    }

    @Override
    public boolean sendRegularMessage(String message) throws IOException {
        synchronized (connectionToTavern){
            return connectionToTavern.sendRegularMessage(message);
        }
    }

    @Override
    public boolean sendWhisperedMessage(String message, String login) throws IOException {
        synchronized (connectionToTavern){
            return connectionToTavern.sendWhisperedMessage(message,login);
        }
    }

    @Override
    public Optional<String> receiveMessage() throws IOException {
        synchronized (connectionToTavern){
            return connectionToTavern.receiveMessage();
        }
    }

    @Override
    public boolean isThereBytesOfPartialMessage() {
        synchronized (connectionToTavern){
            return connectionToTavern.isThereBytesOfPartialMessage();
        }
    }

    @Override
    public void close() {
        synchronized (connectionToTavern){
            connectionToTavern.close();
        }
    }

    @Override
    public boolean isConnected() {
        synchronized (connectionToTavern){
            return connectionToTavern.isConnected();
        }
    }
}
