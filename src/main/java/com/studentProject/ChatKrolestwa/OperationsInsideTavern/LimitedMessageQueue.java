package com.studentProject.ChatKrolestwa.OperationsInsideTavern;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class LimitedMessageQueue implements Queue<String> {
    private final LinkedList<String> internalList;
    private final int maxSize;

    public LimitedMessageQueue(int maxSize) {
        this.internalList = new LinkedList<>();
        this.maxSize = maxSize;
    }

    @Override
    public int size() {
        return internalList.size();
    }

    @Override
    public boolean isEmpty() {
        return internalList.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return internalList.contains(o);
    }

    @Override
    public Iterator<String> iterator() {
        return internalList.iterator();
    }

    public Iterator<String> descendingIterator() {
        return internalList.descendingIterator();
    }

    @Override
    public void forEach(Consumer<? super String> action) {
        internalList.forEach(action);
    }

    @Override
    public Object[] toArray() {
        return internalList.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return internalList.toArray(a);
    }

    @Override
    public boolean add(String playerMessage) {
        if (size() >= maxSize)
            poll();

        return internalList.add(playerMessage);
    }

    @Override
    public boolean remove(Object o) {
        return internalList.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return internalList.contains(c);
    }

    @Override
    public boolean addAll(Collection<? extends String> c) {
        return internalList.addAll(c);
    }


    @Override
    public boolean removeAll(Collection<?> c) {
        return internalList.removeAll(c);
    }

    @Override
    public boolean removeIf(Predicate<? super String> filter) {
        return internalList.removeIf(filter);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return internalList.retainAll(c);
    }

    @Override
    public void clear() {
        internalList.clear();
    }

    @Override
    public Spliterator<String> spliterator() {
        return internalList.spliterator();
    }

    @Override
    public Stream<String> stream() {
        return internalList.stream();
    }

    @Override
    public Stream<String> parallelStream() {
        return internalList.parallelStream();
    }

    @Override
    public boolean offer(String playerMessage) {
        if (size() >= maxSize)
            poll();
        return internalList.offer(playerMessage);
    }

    @Override
    public String remove() {
        return internalList.remove();
    }

    @Override
    public String poll() {
        return internalList.poll();
    }

    @Override
    public String element() {
        return internalList.element();
    }

    @Override
    public String peek() {
        return internalList.peek();
    }
}
