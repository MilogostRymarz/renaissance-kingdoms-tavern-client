package com.studentProject.ChatKrolestwa.OperationsInsideTavern;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class WriteToTavern {
    private final Socket socket;
    private final PrintWriter writeTextToTCP;

    public WriteToTavern(Socket socket) throws IOException {
        this.socket = socket;
        this.writeTextToTCP = new PrintWriter(socket.getOutputStream());
    }

    public boolean sendMessage(String message){


        return false;
    }

    private void sendKeepAliveRequest() {
        final String textOfRequest = "<Message><texte>/keepalive</texte></Message>" + '\0';
        writeTextToTCP.print(textOfRequest);
        writeTextToTCP.flush();
    }


}
