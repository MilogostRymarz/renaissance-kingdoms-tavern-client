package com.studentProject.ChatKrolestwa.OperationsInsideTavern;

import java.io.Closeable;
import java.io.IOException;
import java.util.Optional;

public interface ConnectionToTavern extends Closeable {
    boolean connect();

    void keepAlive() throws IOException;

    boolean sendRegularMessage(String message) throws IOException;

    boolean sendWhisperedMessage(String message, String login) throws IOException;

    Optional<String> receiveMessage() throws IOException;

    boolean isThereBytesOfPartialMessage();

    void close();

    boolean isConnected();
}
