package com.studentProject.ChatKrolestwa.OperationsInsideTavern;


import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Optional;

public class SimpleConnectionToTavern implements ConnectionToTavern {
    private final int tavernId;
    private final String myLogin;
    private final String myPassword;
    private final InetSocketAddress inetSocketAddress;
    private final SocketChannel socketChannel;
    private       byte[] prevReceivedByteArray;

    public SimpleConnectionToTavern(int tavernId, String myLogin, String myPassword, InetSocketAddress inetSocketAddress) throws IOException {
        this.tavernId = tavernId;
        this.myLogin = myLogin;
        //        encoded password
        this.myPassword = myPassword;
        this.inetSocketAddress = inetSocketAddress;
        this.socketChannel = SocketChannel.open();
    }

    public SimpleConnectionToTavern(int tavernId, String myLogin, String myPassword, String serverAddress, int serverPort) throws IOException {
        this.tavernId = tavernId;
        this.myLogin = myLogin;
        //        encoded password
        this.myPassword = myPassword;
        this.inetSocketAddress = new InetSocketAddress(serverAddress,serverPort);
        this.socketChannel = SocketChannel.open();
    }


    @Override
    public boolean connect() {
        if (socketChannel.isConnected())
            return true;
        try {
            if (socketChannel.connect(inetSocketAddress)) {
                this.socketChannel.configureBlocking(false);
                joinSpecifiedTavern();
                return true;
            }
        } catch (IOException e) {
            return false;
        }
        return false;
    }

    @Override
    public void keepAlive() throws IOException {
        if (socketChannel == null || !socketChannel.isConnected())
            throw new IOException();

        sendKeepAliveRequest();
    }

    private void sendKeepAliveRequest() throws IOException {
        final StringBuffer textOfRequest = new StringBuffer("<Message><texte>/keepalive</texte></Message>");
        textOfRequest.append('\0');
        blockingSendRequestToServer(textOfRequest.toString());
    }

    private void joinSpecifiedTavern() throws IOException {
        socketChannel.configureBlocking(false);
        final StringBuilder textRequestToJoinTavern =  new StringBuilder("<Entree><taverneid>");
        textRequestToJoinTavern.append(tavernId);
        textRequestToJoinTavern.append("</taverneid><login>");
        textRequestToJoinTavern.append(myLogin);
        textRequestToJoinTavern.append("</login><pwd>");
        textRequestToJoinTavern.append(myPassword);
        textRequestToJoinTavern.append("</pwd></Entree>");
        textRequestToJoinTavern.append('\0');
        blockingSendRequestToServer(textRequestToJoinTavern.toString());

        final StringBuffer textRequestForMenu =  new StringBuffer("<Message><texte>/menu</texte></Message>");
        textRequestForMenu.append('\0');
        blockingSendRequestToServer(textRequestForMenu.toString());
    }

    private void blockingSendRequestToServer(String message) throws IOException {
        if (socketChannel == null || !socketChannel.isConnected())
            throw new IOException();

        if (message == null || message.length() == 0)
            return;

        final ByteBuffer byteBufferWithMessage = ByteBuffer.wrap(message.getBytes(Charset.forName("UTF-8")));
        do {
            final int sendBytes = socketChannel.write(byteBufferWithMessage);
            if (sendBytes == -1){
                System.err.println("write() in blockingSendRequestToServer() returned -1");
                throw new IOException("write() returned -1");
            } else if (sendBytes == 0)
                Thread.yield();
        } while (byteBufferWithMessage.hasRemaining());
    }

    @Override
    public boolean sendRegularMessage(String message) throws IOException {
        if (socketChannel == null)
            throw new IOException("null in sendRegularMessage()");

        if (message == null || message.length() == 0)
            return false;

        final StringBuilder textToSend = new StringBuilder("<Message><texte>");
        textToSend.append(message);
        textToSend.append("</texte></Message>");
        textToSend.append('\0');

        blockingSendRequestToServer(textToSend.toString());

        return true;
    }

    @Override
    public boolean sendWhisperedMessage(String message, String login) throws IOException {
        if (socketChannel == null || !socketChannel.isConnected())
            throw new IOException();

        if (message == null || message.length() == 0)
            return false;

        final StringBuilder textToSend = new StringBuilder("<Message><texte>/w ");
        textToSend.append(login);
        textToSend.append(' ');
        textToSend.append(message);
        textToSend.append("</texte></Message>");
        textToSend.append('\0');

        blockingSendRequestToServer(textToSend.toString());

        return true;
    }

    /*
    * It should return Optional.empty() or one full transmission
    * */
    @Override
    public Optional<String> receiveMessage() throws IOException {
        byte[] received = receivedBytes();

        if (prevReceivedByteArray != null){
            received = concatenateByteArrays(prevReceivedByteArray,received);
            prevReceivedByteArray = null;
        }
        final byte[] tmpByteArray = received;

        if (received == null || received.length == 0) {
            prevReceivedByteArray = tmpByteArray;
            return Optional.empty();
        }

        final String currentPartOfMessage = new String(received,"UTF-8");
        final String endOfTransmission = "</Transmission>\0";
        int position = -1;
        if ((position = currentPartOfMessage.indexOf(endOfTransmission)) != -1){
            final int remainingCharactersAfterPosition = currentPartOfMessage.length() - position;
            if (remainingCharactersAfterPosition > endOfTransmission.length()){
                prevReceivedByteArray = currentPartOfMessage
                        .substring(position + endOfTransmission.length())
                        .getBytes(Charset.forName("UTF-8"));
            }
            return Optional.of(currentPartOfMessage.substring(0,position + endOfTransmission.length()));
        } else {
            prevReceivedByteArray = tmpByteArray;
            return Optional.empty();
        }
    }

    public boolean isThereBytesOfPartialMessage(){
        if (this.prevReceivedByteArray == null || this.prevReceivedByteArray.length == 0)
            return false;
        return true;
    }

    private byte[] receivedBytes() throws IOException {
        if (socketChannel == null || !socketChannel.isConnected())
            throw new IOException();

        final ByteBuffer receivedByteBuffer = ByteBuffer.allocate(2048);

        final int received = socketChannel.read(receivedByteBuffer);
        if (received == -1)
            throw new IOException("read() returned -1");

        final byte[] bytesToReturn = new byte[received];
        System.arraycopy(receivedByteBuffer.array(),0,bytesToReturn,0,received);

        return bytesToReturn;
    }

    @Override
    public void close() {
        if (socketChannel.isConnected()) {
            try {
                sendRegularMessage("/quitter");
            } catch (IOException e) {

            }

            try {
                socketChannel.close();
            } catch (IOException e) {
                return;
            }
        }
    }

    @Override
    public boolean isConnected() {
        return socketChannel.isConnected();
    }

    private boolean compareByteToChar(byte byteToCompare, char charToCompare){
        final char charFromByte = (char) byteToCompare;
        return (charFromByte == charToCompare);
    }

    private byte[] concatenateByteArrays(byte[] array1, byte[] array2){
        if (array1 == null && array2 == null)
            return null;

        if (array1 == null)
            return array2;

        if (array2 == null)
            return array1;

        int length1 = array1.length;
        int length2 = array2.length;
        final byte[] arrayToReturn = new byte[length1 + length2];
        System.arraycopy(array1,0,arrayToReturn,0,length1);
        System.arraycopy(array2,0,arrayToReturn,length1,length2);
        return arrayToReturn;
    }
}
