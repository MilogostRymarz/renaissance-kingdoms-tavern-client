package com.studentProject.ChatKrolestwa.OperationsInsideTavern;

import java.util.List;

public interface ParsedTavernMenu {
    int          getPriceOfBeer();
    List<String> getNamesOfMeals();
    List<List<Integer>> getIngredientsOfMeals();
    List<Integer> getPricesOfMeals();
}
