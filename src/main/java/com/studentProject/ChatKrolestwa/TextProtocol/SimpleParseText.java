package com.studentProject.ChatKrolestwa.TextProtocol;

import com.studentProject.ChatKrolestwa.OperationsInsideTavern.ParsedPlayerMessage;
import com.studentProject.ChatKrolestwa.OperationsInsideTavern.ParsedPlayerPresence;
import com.studentProject.ChatKrolestwa.OperationsInsideTavern.ParsedTavernMenu;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.swing.text.html.Option;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SimpleParseText {
    private final String unparsedText;
    private final DocumentBuilderFactory factory;

    public SimpleParseText(String text){
        this.unparsedText = removeNullCharacters(text);
        this.factory = DocumentBuilderFactory.newInstance();
    }

    private String removeNullCharacters(String text){
        return text.replaceAll("\u0000", "");
    }

    public Optional<ParsedPlayerMessage> getInterpretedPersonMessage() {
        try (StringReader stringReader = new StringReader(unparsedText);) {
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            InputSource inputSource = new InputSource(stringReader);
            Document document = documentBuilder.parse(inputSource);
            Element rootElement = (Element) document.getDocumentElement();
            if (!isMessage(rootElement))
                return Optional.empty();
            NodeList messageList = rootElement.getElementsByTagName("Message");
            if (messageList != null && messageList.getLength() == 1) {
                Element messageElement = (Element) messageList.item(0);
                final String message = getMessage(messageElement);
                final String author = getAuthorOfMessage(messageElement);
                final String recipient = getRecipientOfMessage(messageElement);
                final int type = getMessageType(messageElement);
                final boolean isHistory = isHistoryMethod(rootElement);
                if (message != null && author != null && type != -1)
                    return Optional.of(new ParsedPlayerMessage() {
                        @Override
                        public String getMessage() {
                            return message;
                        }

                        @Override
                        public int getTypeOfMessage() {
                            return type;
                        }

                        @Override
                        public String getAuthorOfMessage() {
                            return author;
                        }

                        @Override
                        public String getRecipientOfMessage() {
                            return recipient;
                        }

                        @Override
                        public boolean isHistory() {
                            return isHistory;
                        }
                    });
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }


    public Optional<ParsedPlayerPresence> getPersonInTavernInfo(){
        try (StringReader stringReader = new StringReader(unparsedText);) {
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            InputSource inputSource = new InputSource(stringReader);
            Document document = documentBuilder.parse(inputSource);
            Element rootElement = (Element) document.getDocumentElement();
            if (!isPerson(rootElement))
                return Optional.empty();
            NodeList personnageList = rootElement.getElementsByTagName("Personnage");
            Element personElement = (Element) personnageList.item(0);
            final ParsedPlayerPresence.Gender personGender = getGender(personElement);
            final String personName = getPersonLogin(personElement);
            final int    chairNumber = getChairNumber(personElement);
            if (chairNumber != -1 && personName != null && personGender != null)
                return Optional.of(new ParsedPlayerPresence() {
                    @Override
                    public String getPersonName() {
                        return personName;
                    }

                    @Override
                    public int getChairNumber() {
                        return chairNumber;
                    }

                    @Override
                    public Gender getGender() {
                        return personGender;
                    }
                });
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

    public Optional<ParsedTavernMenu> getParsedTavernMenu(){
        try (StringReader stringReader = new StringReader(unparsedText);) {
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            InputSource inputSource = new InputSource(stringReader);
            Document document = documentBuilder.parse(inputSource);
            Element rootElement = (Element) document.getDocumentElement();
            if (!isMenu(rootElement))
                return Optional.empty();
            final Element menuElement = (Element) rootElement.getElementsByTagName("Menu").item(0);
            final int priceOfBeer = getPriceOfBeer(menuElement);
            final Integer numberOfMeals = getAvailableMealsNumber(menuElement);
            if (numberOfMeals == null)
                return Optional.empty();
            final List<String> namesOfMealsList = new ArrayList<>(numberOfMeals);
            final List<List<Integer>> ingredientsOfMealsList = new ArrayList<>(numberOfMeals);
            final List<Integer> pricesOfMealsList = new ArrayList<>(numberOfMeals);
            for (int i = 0; i<numberOfMeals; ++i){
                final String nameOfMeal = getNameOfMeal(menuElement,i);
                final List<Integer> ingredientsOfMeal = getIngredientsOfMeal(menuElement,i);
                final Integer priceOfMeal = getPriceOfMeal(menuElement,i);
                if (nameOfMeal != null && ingredientsOfMeal != null && priceOfMeal != null) {
                    namesOfMealsList.add(nameOfMeal);
                    ingredientsOfMealsList.add(ingredientsOfMeal);
                    pricesOfMealsList.add(priceOfMeal);
                }
            }
            return Optional.of(new ParsedTavernMenu() {

                @Override
                public int getPriceOfBeer() {
                    return priceOfBeer;
                }

                @Override
                public List<String> getNamesOfMeals() {
                    return namesOfMealsList;
                }

                @Override
                public List<List<Integer>> getIngredientsOfMeals() {
                    return ingredientsOfMealsList;
                }

                @Override
                public List<Integer> getPricesOfMeals() {
                    return pricesOfMealsList;
                }
            });
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private List<Integer> getIngredientsOfMeal(Element menuElement, final int i) {
        NodeList mealsList = menuElement.getElementsByTagName("Repas");
        Element mealElement = (Element) mealsList.item(i);
        NodeList ingredientsOfMealList = mealElement.getElementsByTagName("Ingredient");
        final List<Integer> ingredients = new ArrayList<>(ingredientsOfMealList.getLength());
        for (int j = 0; j<ingredientsOfMealList.getLength();++j){
            final Element ingredient = (Element) ingredientsOfMealList.item(j);
            ingredients.add(Integer.valueOf(ingredient.getTextContent()));
        }
        return ingredients;
    }

    private Integer getPriceOfMeal(Element menuElement, int i) {
        NodeList mealsList = menuElement.getElementsByTagName("Repas");
        Element mealElement = (Element) mealsList.item(i);
        NodeList priceOfMealList = mealElement.getElementsByTagName("Prix");
        if (priceOfMealList != null && priceOfMealList.getLength() == 1){
            Element priceOfMeal = (Element) priceOfMealList.item(0);
            Node firstChild = priceOfMeal.getFirstChild();
            if (firstChild != null &&
                    firstChild.getNodeType() == Node.CDATA_SECTION_NODE &&
                    firstChild instanceof CharacterData){
                return Integer.valueOf(((CharacterData) firstChild).getData());
            }
        }
        return null;
    }

    private String getNameOfMeal(Element menuElement, int i) {
        NodeList mealsList = menuElement.getElementsByTagName("Repas");
        Element mealElement = (Element) mealsList.item(i);
        NodeList nameOfMealList = mealElement.getElementsByTagName("Titre");
        if (nameOfMealList != null && nameOfMealList.getLength() == 1){
            Element nameOfMeal = (Element) nameOfMealList.item(0);
            Node firstChild = nameOfMeal.getFirstChild();
            if (firstChild != null &&
                    firstChild.getNodeType() == Node.CDATA_SECTION_NODE &&
                    firstChild instanceof CharacterData){
                return  ((CharacterData) firstChild).getData();
            }
        }
        return null;
    }

    private boolean isMenu(Element rootElement){
        NodeList menuList = rootElement.getElementsByTagName("Menu");
        if (menuList != null && menuList.getLength() == 1)
            return true;
        else
            return false;
    }

    private boolean isHistoryMethod(Element rootElement){
        NodeList historyList = rootElement.getElementsByTagName("Historique");
        if (historyList != null && historyList.getLength() == 1)
            return true;
        else
            return false;
    }

    private int getAvailableMealsNumber(Element menuElement){
        NodeList mealsList = menuElement.getElementsByTagName("Repas");
        return mealsList.getLength();
    }

    private Integer getPriceOfBeer(Element menuElement){
        NodeList beerPriceList = menuElement.getElementsByTagName("PrixBiere");
        if (beerPriceList != null && beerPriceList.getLength() == 1) {
            Element beerPriceElement = (Element) beerPriceList.item(0);
            return Integer.valueOf(beerPriceElement.getTextContent());
        }
        return null;
    }

    private String getAuthorOfMessage(Element messageElement) {
        NodeList loginList = messageElement.getElementsByTagName("login1");
        if (loginList != null && loginList.getLength() == 1) {
            Element loginElement = (Element) loginList.item(0);
            Node firstChild = loginElement.getFirstChild();
            if (firstChild != null && firstChild.getNodeType() == Node.CDATA_SECTION_NODE && firstChild instanceof CharacterData) {
                CharacterData characterData = (CharacterData) firstChild;
                final String dataToReturn = characterData.getData();
                return dataToReturn;
            }
        }

        return null;
    }

    private String getRecipientOfMessage(Element messageElement) {
        NodeList loginList = messageElement.getElementsByTagName("login2");
        if (loginList != null && loginList.getLength() == 1) {
            Element loginElement = (Element) loginList.item(0);
            Node firstChild = loginElement.getFirstChild();
            if (firstChild != null && firstChild.getNodeType() == Node.CDATA_SECTION_NODE && firstChild instanceof CharacterData) {
                CharacterData characterData = (CharacterData) firstChild;
                final String dataToReturn = characterData.getData();
                if (dataToReturn == null)
                    return "";
                return dataToReturn;
            }
        }
        return null;
    }


    private boolean isMessage(Element rootElement) {
        NodeList messageList = rootElement.getElementsByTagName("Message");
        if (messageList != null && messageList.getLength() == 1)
            return true;
        else
            return false;
    }

    private boolean isPerson(Element rootElement){
        NodeList personnageList = rootElement.getElementsByTagName("Personnage");
        return  (personnageList != null && personnageList.getLength() == 1);
    }

    private int getMessageType(Element messageElement) {
        NodeList typeList = messageElement.getElementsByTagName("type");
        if (typeList != null && typeList.getLength() == 1) {
            Element typeElement = (Element) typeList.item(0);
            return Integer.parseInt(typeElement.getTextContent());
        }
        return -1;
    }

    private String getMessage(Element messageElement){
        NodeList textList = messageElement.getElementsByTagName("texte");
        if (textList != null && textList.getLength() == 1){
            Element textElement = (Element) textList.item(0);
            Node firstChild = textElement.getFirstChild();
            if (firstChild != null && firstChild.getNodeType() == Node.CDATA_SECTION_NODE && firstChild instanceof CharacterData){
                CharacterData characterData = (CharacterData) firstChild;
                final String dataToReturn = characterData.getData();
                if (dataToReturn != null)
                    return dataToReturn;
            }
        }
        return null;
    }

    private int getChairNumber(Element personElement){
        NodeList numberList = personElement.getElementsByTagName("num");
        if (numberList != null && numberList.getLength() == 1){
            Element numberElement = (Element) numberList.item(0);
            Node firstChild = numberElement.getFirstChild();
            if (firstChild != null && firstChild.getNodeType() == Node.CDATA_SECTION_NODE && firstChild instanceof CharacterData){
                CharacterData characterData = (CharacterData) firstChild;
                return Integer.parseInt(characterData.getData());
            }
        }
        return -1;
    }

    private String getPersonLogin(Element personElement){
        NodeList loginList = personElement.getElementsByTagName("login");
        if (loginList != null && loginList.getLength() == 1){
            Element loginElement = (Element) loginList.item(0);
            Node firstChild = loginElement.getFirstChild();
            if (firstChild != null && firstChild.getNodeType() == Node.CDATA_SECTION_NODE && firstChild instanceof CharacterData){
                CharacterData characterData = (CharacterData) firstChild;
                return characterData.getData();
            }
        }
        return null;
    }

    private ParsedPlayerPresence.Gender getGender(Element personElement){
        NodeList genderList = personElement.getElementsByTagName("sexe");
        if (genderList != null && genderList.getLength() == 1){
            Element genderElement = (Element) genderList.item(0);
            Node firstChild = genderElement.getFirstChild();
            if (firstChild != null && firstChild.getNodeType() == Node.CDATA_SECTION_NODE && firstChild instanceof CharacterData){
                CharacterData characterData = (CharacterData) firstChild;
                if (characterData.getData().contains("M"))
                    return ParsedPlayerPresence.Gender.men;
                else
                    return ParsedPlayerPresence.Gender.women;
            }
        }
        return null;
    }
}
